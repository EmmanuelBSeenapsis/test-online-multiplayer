using Mirror;
using TMPro;
using UnityEngine;

namespace Seenapsis.Veille.Multiplayer.FPS
{
    public class PlayerController : NetworkBehaviour
    {
        [SyncVar(hook = nameof(OnNameChanged))]
        public string playerName;

        [SyncVar(hook = nameof(OnColorChanged))]
        public Color playerColor = Color.white;

        [SerializeField] private TextMeshPro _playerNameText;
        [SerializeField] private GameObject _floatingInfo;

        private Camera _mainCamera;
        private Renderer _renderer;

        private Transform _mainCameraIdleParent;
        private Vector3 _mainCameraIdleLocalPosition;
        private Quaternion _mainCameraIdleLocalRotation;
        private Material _playerMaterialClone;

        private void Awake()
        {
            _mainCamera = Camera.main;

            _renderer = GetComponent<Renderer>();
        }

        private void Update()
        {
            if (!isLocalPlayer) 
            {
                _floatingInfo.transform.LookAt(_mainCamera.transform);
                return;
            }

            float moveX = Input.GetAxis("Horizontal") * Time.deltaTime * 110.0f;
            float moveZ = Input.GetAxis("Vertical") * Time.deltaTime * 4f;

            transform.Rotate(0, moveX, 0);
            transform.Translate(0, 0, moveZ);
        }

        public override void OnStartLocalPlayer()
        {
            _mainCameraIdleParent = _mainCamera.transform.parent;
            _mainCameraIdleLocalPosition = _mainCamera.transform.localPosition;
            _mainCameraIdleLocalRotation = _mainCamera.transform.localRotation;

            _mainCamera.transform.SetParent(transform);
            _mainCamera.transform.localPosition = Vector3.zero;
            _mainCamera.transform.localRotation = Quaternion.identity;

            _floatingInfo.transform.localPosition = new Vector3(0, -0.3f, 0.6f);
            _floatingInfo.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

            string name = "Player" + Random.Range(100, 999);
            var color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
            CmdSetupPlayer(name, color);
        }

        public override void OnStopClient()
        {
            _mainCamera.transform.SetParent(_mainCameraIdleParent);
            _mainCamera.transform.localPosition = _mainCameraIdleLocalPosition;
            _mainCamera.transform.localRotation = _mainCameraIdleLocalRotation;
        }

        [Command]
        public void CmdSetupPlayer(string name, Color color)
        {
            playerName = name;
            playerColor = color;
        }

        private void OnNameChanged(string oldValue, string newValue)
        {
            _playerNameText.text = playerName;
        }

        private void OnColorChanged(Color oldValue, Color newValue)
        {
            _playerNameText.color = newValue;
            _playerMaterialClone = _renderer.material;
            _playerMaterialClone.color = newValue;
            _renderer.material = _playerMaterialClone;
        }
    }
}