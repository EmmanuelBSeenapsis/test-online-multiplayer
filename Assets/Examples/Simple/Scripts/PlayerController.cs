using UnityEngine;
using Mirror;

namespace Seenapsis.Veille.Multiplayer.Simple
{
    public class PlayerController : NetworkBehaviour
    {
        [SerializeField] private float _walkSpeed;

        private void Update()
        {
            HandleMovement();
        }

        private void HandleMovement()
        {
            if(!isLocalPlayer)
            {
                return;
            }

            float xMovement = Input.GetAxis("Horizontal");
            float yMovement = Input.GetAxis("Vertical");
            var movement = _walkSpeed * new Vector3(xMovement, yMovement, 0);
            transform.position = transform.position + movement;
        }
    }
}
